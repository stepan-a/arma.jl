module arma33

# This module provides functions returning the autocovariance function for AR(1), AR(2), AR(3),
# MA(1), MA(2), MA(3), ARMA(1,1), ARMA(1,2), ARMA(1,3), ARMA(2,1), ARMA(2,2), ARMA(2,3),
# ARMA(3,1), ARMA(1,2), and ARMA(1,3) models. The type of the  MA and/or AR coefficients can
# can be any Number.

export ar1autocov, ar2autocov, ar3autocov
export ma1autocov, ma2autocov, ma3autocov
export arma11autocov, arma12autocov, arma13autocov
export arma21autocov, arma22autocov, arma23autocov
export arma31autocov, arma32autocov, arma33autocov

function omega{T}(ar::Vector{T}, ma::Vector{T}, sigma2::T)
    ω0 = sigma2
    ω1 = (ar[1]+ma[1])*ω0
    ω2 = ar[1]*ω1 + (ar[2]+ma[2])*ω0
    ω3 = ar[1]*ω2 + ar[2]*ω1 + (ar[3]+ma[3])*ω0
    return ω0, ω1, ω2, ω3
end

function b{T}(ar::Vector{T}, ma::Vector{T}, sigma2::T)
    ω0, ω1, ω2, ω3 = omega(ar, ma, sigma2)
    b0 = ω0 + ma[1]*ω1 + ma[2]*ω2 + ma[3]*ω3
    b1 = ma[1]*ω0 + ma[2]*ω1 + ma[3]*ω2
    b2 = ma[2]*ω0 + ma[3]*ω1
    b3 = ma[3]*ω0
    return b0, b1, b2, b3
end

function c{T}(ar::Vector{T}, ma::Vector{T}, sigma2::T)
    b0, b1, b2, b3 = b(ar, ma, sigma2)
    c1 = (ar[3]*b2+b1)/(1-ar[2]-ar[3]*(ar[1]+ar[3]))
    c2 = (ar[1]+ar[3])*c1+b2
    c3 = ar[1]*c2 + ar[2]*c1 + b3
    c0 = ar[1]*c1+ar[2]*c2+ar[3]*c3+b0
    return c0, c1, c2, c3
end

function autocovarianceinit{T}(ar::Vector{T}, ma::Vector{T}, sigma2::T)
    δ1 = (ar[1]+ar[2]*ar[3])/(1-ar[2]-ar[3]*(ar[1]+ar[3]))
    δ2 = δ1*(ar[1]+ar[3])+ar[2]
    δ3 = ar[1]*δ2 + ar[2]*δ1 + ar[3]
    c0, c1, c2, c3 = c(ar, ma, sigma2)
    γ0 = c0/(1-ar[1]*δ1-ar[2]*δ2-ar[3]*δ3)
    γ1 = δ1*γ0 + c1
    γ2 = δ2*γ0 + c2
    γ3 = δ3*γ0 + c3
    ac = zeros(T,4)
    ac[1] = γ0
    ac[2] = γ1
    ac[3] = γ2
    ac[4] = γ3
    return ac
end

function ar1autocov{T}(p1::T, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=5:maxorder
        ac[t] = ar[1]*ac[t-1]
    end
    return ac
end

function ar2autocov{T}(p1::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1[1]
    ar[2] = p1[2]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=5:maxorder
        ac[t] = ar[1]*ac[t-1] + ar[2]*ac[t-2]
    end
    return ac
end

function ar3autocov{T}(p1::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1[1]
    ar[2] = p1[2]
    ar[3] = p1[3]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=5:maxorder
        ac[t] = ar[1]*ac[t-1] + ar[2]*ac[t-2] + ar[3]*ac[t-3]
    end
    return ac
end

function ma1autocov{T}(p2::T, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ma[1] = p2
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    return ac
end

function ma2autocov{T}(p2::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ma[1] = p2[1]
    ma[2] = p2[2]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    return ac
end

function ma3autocov{T}(p2::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ma[1] = p2[1]
    ma[2] = p2[2]
    ma[3] = p2[3]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    return ac
end

function arma11autocov{T}(p1::T, p2::T, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1
    ma[1] = p2
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=3:maxorder
        ac[t] = ar[1]*ac[t-1]
    end
    return ac
end

function arma12autocov{T}(p1::T, p2::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1
    ma[1] = p2[1]
    ma[2] = p2[2]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=4:maxorder
        ac[t] = ar[1]*ac[t-1]
    end
    return ac
end

function arma13autocov{T}(p1::T, p2::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1
    ma[1] = p2[1]
    ma[2] = p2[2]
    ma[3] = p2[3]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=5:maxorder
        ac[t] = ar[1]*ac[t-1]
    end
    return ac
end

function arma21autocov{T}(p1::Vector{T}, p2::T, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1[1]
    ar[2] = p1[2]
    ma[1] = p2
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=4:maxorder
        ac[t] = ar[1]*ac[t-1] + ar[2]*ac[t-2]
    end
    return ac
end

function arma22autocov{T}(p1::Vector{T}, p2::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1[1]
    ar[2] = p1[2]
    ma[1] = p2[1]
    ma[2] = p2[2]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=4:maxorder
        ac[t] = ar[1]*ac[t-1] + ar[2]*ac[t-2]
    end
    return ac
end

function arma23autocov{T}(p1::Vector{T}, p2::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1[1]
    ar[2] = p1[2]
    ma[1] = p2[1]
    ma[2] = p2[2]
    ma[3] = p2[3]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=5:maxorder
        ac[t] = ar[1]*ac[t-1] + ar[2]*ac[t-2] + ar[3]*ac[t-3]
    end
    return ac
end

function arma31autocov{T}(p1::Vector{T}, p2::T, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1[1]
    ar[2] = p1[2]
    ar[3] = p1[3]
    ma[1] = p2
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=5:maxorder
        ac[t] = ar[1]*ac[t-1] + ar[2]*ac[t-2]
    end
    return ac
end

function arma32autocov{T}(p1::Vector{T}, p2::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1[1]
    ar[2] = p1[2]
    ar[3] = p1[3]
    ma[1] = p2[1]
    ma[2] = p2[2]
    ac = zeros(T, maxorder)
    ac[1:5] = autocovarianceinit(ar, ma, sigma2)
    for t=4:maxorder
        ac[t] = ar[1]*ac[t-1] + ar[2]*ac[t-2]
    end
    return ac
end

function arma33autocov{T}(p1::Vector{T}, p2::Vector{T}, sigma2::T, maxorder::Int = 10)
    ar = zeros(T,3)
    ma = zeros(T,3)
    ar[1] = p1[1]
    ar[2] = p1[2]
    ar[3] = p1[3]
    ma[1] = p2[1]
    ma[2] = p2[2]
    ma[3] = p2[3]
    ac = zeros(T, maxorder)
    ac[1:4] = autocovarianceinit(ar, ma, sigma2)
    for t=5:maxorder
        ac[t] = ar[1]*ac[t-1] + ar[2]*ac[t-2] + ar[3]*ac[t-3]
    end
    return ac
end

end
