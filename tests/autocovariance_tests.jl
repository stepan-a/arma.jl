include("../src/arma.jl")
include("./arma33.jl")

using armamodels
using arma33
using Base.Test


# MA(1)
theta = Parameters([.5])
model = ma(theta, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ma1autocov(model.theta[1],1.0)

# MA(2)
theta = Parameters([.5, -.1])
model = ma(theta, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ma2autocov(model.theta.params,1.0)

# MA(3)
theta = Parameters([.5, -.1, .2])
model = ma(theta, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ma3autocov(model.theta.params,1.0)

# AR(1)
phi = Parameters([.5])
model = ar(phi, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar1autocov(model.phi[1],1.0)

# AR(2)
phi = Parameters([.5, -.1])
model = ar(phi, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar2autocov(model.phi.params,1.0)

# AR(3)
phi = Parameters([.0, .0, .5])
model = ar(phi, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar3autocov(model.phi.params,1.0)


# ARMA(1,1)
phi = Parameters([.5])
theta = Parameters([.1])
model = arma(phi, theta, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma arma11autocov(model.phi[1],model.theta[1],1.0)

