include("../src/arma.jl")
include("./arma33.jl")

using armamodels
using arma33
using Base.Test


# MA(1)
θ = Parameters([.5])
model = ma(θ, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ma1autocov(model.θ[1],1.0)

# MA(2)
θ = Parameters([.5, -.1])
model = ma(θ, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ma2autocov(model.θ.params,1.0)

# MA(3)
θ = Parameters([.5, -.1, .2])
model = ma(θ, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ma3autocov(model.θ.params,1.0)

# AR(1)
φ = Parameters([.5])
model = ar(φ, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar1autocov(model.φ[1],1.0)

# AR(2)
φ = Parameters([.5, -.1])
model = ar(φ, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar2autocov(model.φ.params,1.0)

# AR(3)
φ = Parameters([.0, .0, .5])
model = ar(φ, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar3autocov(model.φ.params,1.0)

# ARMA(1,1)
φ = Parameters([.5])
θ = Parameters([.1])
model = arma(φ, θ, 1.0)
show(model)
gamma = zeros(Float64, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma arma11autocov(model.φ[1],model.θ[1],1.0)

# AR(1) with rational parameters
φ = Parameters([1//2])
model = ar(φ, 1//1)
show(model)
gamma = zeros(Rational{Int64}, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar1autocov(model.φ[1],1//1)

# AR(2) with rational parameters
φ = Parameters([1//2, -1//4])
model = ar(φ, 1//1)
show(model)
gamma = zeros(Rational{Int64}, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar2autocov(model.φ.params,1//1)

# AR(3) with rational parameters
φ = Parameters([1//2, -1//4, 1//3])
model = ar(φ, 1//1)
show(model)
gamma = zeros(Rational{Int64}, 10)
autocovariance(model, gamma, true)
@test_approx_eq gamma ar3autocov(model.φ.params,1//1)
