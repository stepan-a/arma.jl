include("../src/arma.jl")
include("./arma33.jl")

using armamodels
using arma33
using Base.Test

# AR(3)
phi = Parameters([.0, .0, .5])
model = ar(phi, 1.0)
show(model)
yinit = zeros(Float64, 3)
T = 1000000
y = zeros(Float64, T)

armamodels.simulate(model, yinit, y)

println(dot(y,y)/T)
println(dot(y[2:T],y[1:T-1])/T)
println(dot(y[3:T],y[1:T-2])/T)
println(dot(y[4:T],y[1:T-3])/T)
