module armamodels

import Base.length, Base.show, Base.getindex, Base.getfield

export Parameters, ar, ma, arma, autocovariance


"""
    Parameters{S<:Real}

Parameterized type for autoregressive or moving average parameters of an ARMA(p,q) model. By convention, the first autoregressive and moving average parameters are set equal to one.
"""
type Parameters{S<:Real}
    "Vector of parameters for lags ```1``` to ```r```."
    params::Vector{S}
end


"""
    length{S}(p::Parameters{S})

Overloads the length method for Parameters type.
"""
function length{S}(p::Parameters{S})
    Base.length(p.params)
end

"""
    getindex{S}(p::Parameters{S}, i:Int)

Overloads the getindex method for Parameters type. Returns the coefficient associated to lag ```i```.
"""
function getindex{S}(p::Parameters{S}, i::Int)
    if i==0
        return one(S)
    end
    if i<0 || i>length(p.params)
        return zero(S)
    else
        return p.params[i]
    end
end

"""
    arma{S<:Real}

Parameterized type for ARMA(p,q) models.
"""
type arma{S<:Real}
    "Autoregressive parameters"
    φ::Parameters{S}
    "Moving average parameters"
    θ::Parameters{S}
    "Variance of the innovations"
    σ²::S
    "Number of lags in the autoregressive part"
    p::Int
    "Number of lags in the moving average part"
    q::Int
    "Name of the endogenous variable"
    yname::String
    "Name of the innovation"
    iname::String
    "Constructor for ARMA(p,q) models, with default names for the variables"
    function arma{S}(φ::Parameters{S}, θ::Parameters{S}, σ²::S)
        new(φ, θ, σ², length(φ), length(θ), "y", "e")
    end
    function arma{S}(φ::Parameters{S}, θ::Parameters{S}, σ²::S, yname::String, iname::String)
        new(φ, θ, σ², length(φ), length(θ), yname, iname)
    end
end

type ma{S}
    θ::Parameters{S}
    σ²::S
    q::Int
    yname::String
    iname::String
    function ma{S}(θ::Parameters{S}, σ²::S)
        new(θ, σ², length(θ), "y", "e")
    end
    function ma{S}(θ::Parameters{S}, σ²::S, yname::String, iname::String)
        new(ϕ, θ, σ², length(θ), yname, iname)
    end
end

type ar{S}
    φ::Parameters{S}
    σ²::S
    p::Int
    yname::String
    iname::String
    function ar{S}(φ::Parameters{S}, σ²::S)
        new(φ, σ², length(φ), "y", "e")
    end
    function ar{S}(φ::Parameters{S}, σ²::S, yname::String, iname::String)
        new(φ, σ², length(φ), yname, iname)
    end
end

ar{T}(φ::Parameters{T}, σ²::T) = ar{T}(φ, σ²)
ma{T}(θ::Parameters{T}, σ²::T) = ma{T}(θ, σ²)
arma{T}(φ::Parameters{T}, θ::Parameters{T}, σ²::T) = arma{T}(φ, θ, σ²)

ar{T}(φ::Parameters{T}, σ²::T, yname::String, iname::String) = ar{T}(φ, σ², yname, iname)
ma{T}(θ::Parameters{T}, σ²::T, yname::String, iname::String) = ma{T}(θ, σ², yname, iname)
arma{T}(φ::Parameters{T}, θ::Parameters{T}, σ²::T, yname::String, iname::String) = arma{T}(φ, θ, σ², yname, iname)

##
## ROUTINES FOR DISPLAYING ARMA MODEL
##

function show{T}(io::IO, model::Union{arma{T}, ar{T}, ma{T}})
    str1, str2 = "\nGaussian ", string("model with innovation variance equal to ", string(model.σ²), ":\n")
    if isa(model,arma{T})
        println(io, str1, "ARMA(", string(model.p), ",", string(model.q), ") ", str2)
    elseif isa(model,ar{T})
        println(io, str1, "AR(", string(model.p), ") ", str2)
    elseif isa(model,ma{T})
        println(io, str1, "MA(", string(model.q), ") ", str2)
    end
    str = string(model.yname, "(t) = ")
    if isa(model,Union{arma{T}, ar{T}})
        str = writeautoregressivepart(model, str)
    end
    if isa(model,ma{T})
        str = string(str, model.iname, "(t)")
    else
        str = string(str, " + ", model.iname, "(t)")
    end
    if isa(model,Union{arma{T}, ma{T}})
        str = writemovingaveragepart(model, str)
    end
    println(io, "\n\t", str, "\n")
end

function writeautoregressivepart{T}(model::Union{arma{T}, ar{T}}, str::String)
    tmp = str
    for i=1:model.p
        if i>1
            tmp = string(tmp, abs(model.φ[i]))
        else
            tmp = string(tmp, model.φ[1])
        end
        tmp = string(tmp, model.yname, "(t-")
        tmp = string(tmp, string(i))
        tmp = string(tmp, ")")
        if i<model.p && model.φ[i+1]<0
            tmp = string(tmp," - ")
        elseif i<model.p
            tmp = string(tmp," + ")
        end
    end
    return tmp
end

function writemovingaveragepart{T}(model::Union{arma{T}, ma{T}}, str::String)
    tmp = str
    for i=1:model.q
        if model.θ[i]<0
            tmp = string(tmp, " - ")
        else
            tmp = string(tmp, " + ")
        end
        tmp = string(tmp, string(abs(model.θ[i])))
        tmp = string(tmp, model.iname, "(t-")
        tmp = string(tmp, string(i), ")")
    end
    return tmp
end

##
## ROUTINES FOR SIMULATING ARMA MODELS
##

function simulate{S}(model::ar{S}, yinit::Vector{S}, y::Vector{S})
    # Simulates a Gaussian AR(p) model.
    #
    # model [ar]               contains the definition of the ARMA model (parameters).
    # yinit [vector{S}]  p initial conditions (y_{0}, y_{-1}, ..., y_{-p+1}) for the endogenous variable.
    # y     [vector{S}]  simulated data.
    #
    # The size of the simulated sample is decided by the size of the vector y.
    T = length(y)
    ybuffer = copy(yinit)
    σ = sqrt(model.σ²)
    for t=1:T
        y[t] = σ*randn()
        for l=1:model.p
            y[t] += model.φ[l]*ybuffer[l]
        end
        ybuffer[2:model.p] = ybuffer[1:model.p-1]
        ybuffer[1] = y[t]
    end
end

function simulate{S}(model::ma{S}, einit::Vector{S}, y::Vector{S})
    # Simulates a Gaussian MA(q) model.
    #
    # model [ma]               contains the definition of the ARMA model (parameters).
    # einit [vector{Float64}]  q initial conditions (e_{0}, e_{-1}, ..., e_{-q+1}) for the innovations.
    # y     [vector{Float64}]  simulated data.
    #
    # The size of the simulated sample is decided by the size of the vector y.
    T = length(y)
    ebuffer = copy(einit)
    σ = sqrt(model.σ²)
    for t=1:T
        e = σ*randn()
        y[t] = e[1]
        for l=1:model.q
            y[t] += model.θ[l]*ebuffer[l]
        end
        ebuffer[2:model.q] = ebuffer[1:model.q-1]
        ebuffer[1] = e[1]
    end
end

function simulate{S}(model::arma{S}, yinit::Vector{S}, einit::Vector{S}, y::Vector{S})
    # Simulates a Gaussian ARMA(p,q) model.
    #
    # model [arma]             contains the definition of the ARMA model (parameters).
    # yinit [vector{Float64}]  p initial conditions (y_{0}, y_{-1}, ..., y_{-p+1}) for the endogenous variable.
    # einit [vector{Float64}]  q initial conditions (e_{0}, e_{-1}, ..., e_{-q+1}) for the innovations.
    # y     [vector{Float64}]  simulated data.
    #
    # The size of the simulated sample is decided by the size of the vector y.
    T = length(y)
    ybuffer = copy(yinit)
    ebuffer = copy(einit)
    σ = sqrt(model.σ²)
    for t=1:T
        e = σ*randn()
        y[t] = e[1]
        if model.p>0
            for l=1:model.p
                y[t] += model.φ[l]*ybuffer[l]
            end
            ybuffer[2:model.p] = ybuffer[1:model.p-1]
        end
        if model.q>0
            for l=1:model.q
                y[t] += model.θ[l]*ebuffer[l]
            end
            ebuffer[2:model.q] = ebuffer[1:model.q-1]
            ebuffer[1] = e[1]
        end
        if model.p>0
            ybuffer[1] = y[t]
        end
    end
end

##
## ROUTINES FOR COMPUTING SECOND ORDER MOMENTS
##

function autocovariance{S}(model::ma{S}, gamma::Vector{S}, display::Bool = false)
    # Computes the autocovariance function of an MA(q) stochastic process.
    for h=0:min(length(gamma)-1,model.q)
        tmp = zero(S)
        for i=0:length(model.θ)
            tmp += model.θ[i]*model.θ[i-h]
        end
        gamma[h+1] = tmp
    end
    if length(gamma)-1>model.q
        gamma[model.q+2:end] = zero(S)
    end
    if display
        for h=0:length(gamma)-1
            println("gamma(",  h,") = ", gamma[h+1])
        end
    end
end

function autocovariance{S}(model::Union{ar{S}, arma{S}}, gamma::Vector{S}, display::Bool = false)
    # Computes the autocovariance function of an AR(p) stochastic process by solving the Yule Walker equations.
    b = getYuleWalkerVector(model)
    A = getYuleWalkerMatrix(model)
    # Compute the variance and max(p,q) first autocovariances.
    g = A\b
    if length(gamma)<length(g)
        gamma = g[1:length(gamma)]
    else
        # Computes the higher order autocovariances.
        gamma[1:length(g)] = g
        for h=length(g)+1:length(gamma)
            gamma[h] = zero(S)
            for lag=1:model.p
                gamma[h] += model.φ[lag]*gamma[h-lag]
            end
        end
    end
    if display
        for h=0:length(gamma)-1
            println("gamma(",  h,") = ", gamma[h+1])
        end
    end
end

function getYuleWalkerMatrix{T}(model::Union{ar{T}, arma{T}})
    if isa(model,arma{T})
        n = max(model.p,model.q)+1
    else
        n = model.p+1
    end
    yulewalkermatrix = zeros(T, n, n)
    # Row 1.
    yulewalkermatrix[1,1], lag = one(T), 1
    for c=2:model.p+1
        yulewalkermatrix[1,c] = -model.φ[lag]
        lag += 1
    end
    # Row p+1.
    yulewalkermatrix[model.p+1,model.p+1], lag = one(T), model.p
    for c=1:model.p
        yulewalkermatrix[model.p+1,c] = -model.φ[lag]
        lag -= 1
    end
    # Rows 2 to p.
    if model.p>1
        for lag=1:model.p-1
            r = lag+1
            yulewalkermatrix[r,1] = -model.φ[lag]
            shift = 0
            if r>2
                for c=2:r
                    shift += 1
                    yulewalkermatrix[r,c] = -(model.φ[lag-shift]+model.φ[lag+shift])
                end
            end
            baseindex = 2*lag
            yulewalkermatrix[r,r] = 1-model.φ[baseindex]
            for c = r+1:model.p+1
                baseindex += 1
                if baseindex<=model.p
                    yulewalkermatrix[r,c] = -model.φ[baseindex]
                end
            end
        end
    end
    # Rows p+2 to n
    if model.p+1<n
        for r=model.p+2:n
            yulewalkermatrix[r,r], lag = one(T), 1
            for c=r-1:r-model.p
                yulewalkermatrix[r,c] = -model.φ[lag]
                lag += 1
            end
        end
    end
    return yulewalkermatrix
end

function getYuleWalkerVector{T}(model::ar{T})
    ω = zeros(T,model.p+1)
    ω[1] = model.σ²
    return ω
end

function getYuleWalkerVector{T}(model::arma{T})
    n = max(model.p, model.q)+1
    ω = zeros(T, n)
    α = getalpha(model, model.q)
    for line=1:model.q+1
        h = line-1
        for j=0:model.q-h
            ω[line] += model.θ[h+j]*α[1+j]
        end
    end
    return ω
end

function getalpha{T}(model::arma{T}, n::Int)
    α = zeros(T, n+1)
    α[1] = model.σ²
    for i=2:n+1
        for h=1:min(i-1,model.p)
            α[i] += model.φ[h]*α[i-h]+model.θ[h]*model.σ²
        end
    end
    return α
end

end
