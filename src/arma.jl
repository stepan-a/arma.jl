module armamodels

import Base.length, Base.show

export Parameters, ar, ma, arma, autocovariance

type Parameters{S}
    params::Vector{S}
end

function length{S}(p::Parameters{S})
    Base.length(p.params)
end

function getindex{S}(p::Parameters{S}, i::Int)
    if i==0
        return one(S)
    end
    if i<0 || i>length(p.params)
        return zero(S)
    else
        return p.params[i]
    end
end

type arma{S}
    phi::Parameters{S}
    theta::Parameters{S}
    sigma2::S
    p::Int
    q::Int
    yname::String
    iname::String
    function arma{S}(phi::Parameters{S}, theta::Parameters{S}, sigma2::S)
        new(phi, theta, sigma2, length(phi), length(theta), "y", "e")
    end
    function arma{S}(phi::Parameters{S}, theta::Parameters{S}, sigma2::S, yname::String, iname::String)
        new(phi, theta, sigma2, length(phi), length(theta), yname, iname)
    end
end

type ma{S}
    theta::Parameters{S}
    sigma2::S
    q::Int
    yname::String
    iname::String
    function ma{S}(theta::Parameters{S}, sigma2::S)
        new(theta, sigma2, length(theta), "y", "e")
    end
    function ma{S}(theta::Parameters{S}, sigma2::S, yname::String, iname::String)
        new(phi, theta, sigma2, length(phi), length(theta), yname, iname)
    end
end

type ar{S}
    phi::Parameters{S}
    sigma2::S
    p::Int
    yname::String
    iname::String
    function ar{S}(phi::Parameters{S}, sigma2::S)
        new(phi, sigma2, length(phi), "y", "e")
    end
    function ar{S}(phi::Parameters{S}, sigma2::S, yname::String, iname::String)
        new(phi, sigma2, length(phi), length(theta), yname, iname)
    end
end

ar{T}(phi::Parameters{T}, sigma2::T) = ar{T}(phi, sigma2)
ma{T}(theta::Parameters{T}, sigma2::T) = ma{T}(theta, sigma2)
arma{T}(phi::Parameters{T}, theta::Parameters{T}, sigma2::T) = arma{T}(phi, theta, sigma2)

ar{T}(phi::Parameters{T}, sigma2::T, yname::String, iname::String) = ar{T}(phi, sigma2, yname, iname)
ma{T}(theta::Parameters{T}, sigma2::T, yname::String, iname::String) = ma{T}(theta, sigma2, yname, iname)
arma{T}(phi::Parameters{T}, theta::Parameters{T}, sigma2::T, yname::String, iname::String) = arma{T}(phi, theta, sigma2, yname, iname)

function show{T}(io::IO, model::Union(arma{T}, ar{T}, ma{T}))
    str1, str2 = "\nGaussian ", string("model with innovation variance equal to ", string(model.sigma2), ":\n")
    if isa(model,arma{T})
        println(io, str1, "ARMA(", string(model.p), ",", string(model.q), ") ", str2)
    elseif isa(model,ar{T})
        println(io, str1, "AR(", string(model.p), ") ", str2)
    elseif isa(model,ma{T})
        println(io, str1, "MA(", string(model.q), ") ", str2)
    end
    str = string(model.yname, "(t) = ")
    if isa(model,Union(arma{T},ar{T}))
        str = writeautoregressivepart(model, str)
    end
    if isa(model,ma{T})
        str = string(str, model.iname, "(t)")
    else
        str = string(str, " + ", model.iname, "(t)")
    end
    if isa(model,Union(arma{T},ma{T}))
        str = writemovingaveragepart(model, str)
    end
    println(io, "\n\t", str, "\n")
end

function writeautoregressivepart{T}(model::Union(arma{T},ar{T}), str::String)
    tmp = str
    for i=1:model.p
        if i>1
            tmp = string(tmp, abs(model.phi[i]))
        else
            tmp = string(tmp, model.phi[1])
        end
        tmp = string(tmp, model.yname, "(t-")
        tmp = string(tmp, string(i))
        tmp = string(tmp, ")")
        if i<model.p && model.phi[i+1]<0
            tmp = string(tmp," - ")
        elseif i<model.p
            tmp = string(tmp," + ")
        end
    end
    return tmp
end

function writemovingaveragepart{T}(model::Union(arma{T},ma{T}), str::String)
    tmp = str
    for i=1:model.q
        if model.theta[i]<0
            tmp = string(tmp, " - ")
        else
            tmp = string(tmp, " + ")
        end
        tmp = string(tmp, string(abs(model.theta[i])))
        tmp = string(tmp, model.iname, "(t-")
        tmp = string(tmp, string(i), ")")
    end
    return tmp
end

function simulate{S}(model::ar{S}, yinit::Vector{S}, y::Vector{S})
    # Simulates a Gaussian AR(p) model.
    #
    # model [ar]               contains the definition of the ARMA model (parameters).
    # yinit [vector{S}]  p initial conditions (y_{0}, y_{-1}, ..., y_{-p+1}) for the endogenous variable.
    # y     [vector{S}]  simulated data.
    #
    # The size of the simulated sample is decided by the size of the vector y.
    T = length(y)
    ybuffer = copy(yinit)
    sigma = sqrt(model.sigma2)
    for t=1:T
        y[t] = (sigma*randn(1))[1]
        for l=1:model.p
            y[t] += model.phi[l]*ybuffer[l]
        end
        ybuffer[2:model.p] = ybuffer[1:model.p-1]
        ybuffer[1] = y[t]
    end
end

function simulate{S}(model::ma{S}, einit::Vector{S}, y::Vector{S})
    # Simulates a Gaussian MA(q) model.
    #
    # model [ma]               contains the definition of the ARMA model (parameters).
    # einit [vector{Float64}]  q initial conditions (e_{0}, e_{-1}, ..., e_{-q+1}) for the innovations.
    # y     [vector{Float64}]  simulated data.
    #
    # The size of the simulated sample is decided by the size of the vector y.
    T = length(y)
    ebuffer = copy(einit)
    sigma = sqrt(model.sigma2)
    for t=1:T
        e = sigma*randn(1)
        y[t] = e[1]
        for l=1:model.q
            y[t] += model.theta[l]*ebuffer[l]
        end
        ebuffer[2:model.q] = ebuffer[1:model.q-1]
        ebuffer[1] = e[1]
    end
end

function simulate{S}(model::arma{S}, yinit::Vector{S}, einit::Vector{S}, y::Vector{S})
    # Simulates a Gaussian ARMA(p,q) model.
    #
    # model [arma]             contains the definition of the ARMA model (parameters).
    # yinit [vector{Float64}]  p initial conditions (y_{0}, y_{-1}, ..., y_{-p+1}) for the endogenous variable.
    # einit [vector{Float64}]  q initial conditions (e_{0}, e_{-1}, ..., e_{-q+1}) for the innovations.
    # y     [vector{Float64}]  simulated data.
    #
    # The size of the simulated sample is decided by the size of the vector y.
    T = length(y)
    ybuffer = copy(yinit)
    ebuffer = copy(einit)
    sigma = sqrt(model.sigma2)
    for t=1:T
        e = sigma*randn(1)
        y[t] = e[1]
        if model.p>0
            for l=1:model.p
                y[t] += model.phi[l]*ybuffer[l]
            end
            ybuffer[2:model.p] = ybuffer[1:model.p-1]
        end
        if model.q>0
            for l=1:model.q
                y[t] += model.theta[l]*ebuffer[l]
            end
            ebuffer[2:model.q] = ebuffer[1:model.q-1]
            ebuffer[1] = e[1]
        end
        if model.p>0
            ybuffer[1] = y[t]
        end
    end
end

function autocovariance{S}(model::ma{S}, gamma::Vector{S}, display::Bool = false)
    # Computes the autocovariance function of an MA(q) stochastic process.
    for h=0:min(length(gamma)-1,model.q)
        tmp = zero(S)
        for i=0:length(model.theta)
            tmp += model.theta[i]*model.theta[i-h]
        end
        gamma[h+1] = tmp
    end
    if length(gamma)-1>model.q
        gamma[model.q+2:end] = zero(S)
    end
    if display
        for h=0:length(gamma)-1
            println("gamma(",  h,") = ", gamma[h+1])
        end
    end
end

function autocovariance{S}(model::Union(ar{S},arma{S}), gamma::Vector{S}, display::Bool = false)
    # Computes the autocovariance function of an AR(p) stochastic process by solving the Yule Walker equations.
    b = getYuleWalkerVector(model)
    A = getYuleWalkerMatrix(model)
    # Compute the variance and max(p,q) first autocovariances.
    g = A\b
    if length(gamma)<length(g)
        gamma = g[1:length(gamma)]
    else
        # Computes the higher order autocovariances.
        gamma[1:length(g)] = g
        for h=length(g)+1:length(gamma)
            gamma[h] = zero(S)
            for lag=1:model.p
                gamma[h] += model.phi[lag]*gamma[h-lag]
            end
        end
    end
    if display
        for h=0:length(gamma)-1
            println("gamma(",  h,") = ", gamma[h+1])
        end
    end
end

function getYuleWalkerMatrix{T}(model::Union(ar{T}, arma{T}))
    if isa(model,arma{T})
        n = max(model.p,model.q)+1
    else
        n = model.p+1
    end
    yulewalkermatrix = zeros(T, n, n)
    # Row 1.
    yulewalkermatrix[1,1], lag = one(T), 1
    for c=2:model.p+1
        yulewalkermatrix[1,c] = -model.phi[lag]
        lag += 1
    end
    # Row p+1.
    yulewalkermatrix[model.p+1,model.p+1], lag = one(T), model.p
    for c=1:model.p
        yulewalkermatrix[model.p+1,c] = -model.phi[lag]
        lag -= 1
    end
    # Rows 2 to p.
    if model.p>1
        for lag=1:model.p-1
            r = lag+1
            yulewalkermatrix[r,1] = -model.phi[lag]
            shift = 1
            if r>2
                for c=2:r
                    yulewalkermatrix[r,c] = -(model.phi[lag-shift]+model.phi[lag+shift])
                    shift += 1
                end
            end
            yulewalkermatrix[r,r] = 1-model.phi[lag+shift]
            for c = r+2:model.p+1
                shift += 1
                yulewalkermatrix[r,c] = -model.phi[lag+shift]
            end
        end
    end
    # Rows p+2 to n
    if model.p+1<n
        for r=model.p+2:n
            yulewalkermatrix[r,r], lag = one(T), 1
            for c=r-1:r-model.p
                yulewalkermatrix[r,c] = -model.phi[lag]
                lag += 1
            end
        end
    end
    return yulewalkermatrix
end

function getYuleWalkerVector{T}(model::ar{T})
    w = zeros(T,model.p+1)
    w[1] = model.sigma2
    return w
end

function getYuleWalkerVector{T}(model::arma{T})
    n = max(model.p, model.q)+1
    w = zeros(T, n)
    a = getalpha(model, model.q)
    for line=1:model.q+1
        h = line-1
        for j=0:model.q-h
            w[line] += model.theta[h+j]*a[1+j]
        end
    end
    return w
end

function getalpha{T}(model::arma{T}, n::Int)
    a = zeros(T, n+1)
    a[1] = model.sigma2
    for i=2:n+1
        for h=1:min(i-1,model.p)
            a[i] += model.phi[h]*a[i-h]+model.theta[h]*model.sigma2
        end
    end
    return a
end

end
